import { Component, OnInit } from '@angular/core';
import { LoggedInCallback, CognitoUtil } from 'src/app/service/cognito.service';
import { UserLoginService } from 'src/app/service/user-login.service';
import { AwsUtil } from 'src/app/service/aws.service';

@Component({
  selector: 'jd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, LoggedInCallback {
  constructor(public awsUtil: AwsUtil, public userService: UserLoginService, public cognito: CognitoUtil) {
    console.log('AppComponent: constructor');
}

ngOnInit() {
    console.log('AppComponent: Checking if the user is already authenticated');
    this.userService.isAuthenticated(this);
}

isLoggedIn(message: string, isLoggedIn: boolean) {
    console.log('AppComponent: the user is authenticated: ' + isLoggedIn);
    const mythis = this;
    this.cognito.getIdToken({
        callback() {

        },
        callbackWithParam(token: any) {
            // Include the passed-in callback here as well so that it's executed downstream
            console.log('AppComponent: calling initAwsService in callback ' + token);
            mythis.awsUtil.initAwsService(null, isLoggedIn, token);
        }
    });
}
}
