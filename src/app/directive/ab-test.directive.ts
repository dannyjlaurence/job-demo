import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[jdAbTest]'
})
export class AbTestDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
