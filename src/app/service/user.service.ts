import { Injectable, Type } from '@angular/core';
import { CognitoUtil } from './cognito.service';
import { BehaviorSubject } from 'rxjs';

export enum Event {
  viewJob,
  startApplication,
  finishApplication,
  confirmApplication
}

export interface EventItem {
  abId?: string,
  jobId: number,
  event: Event,
  timestamp: Date;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  events: EventItem[] = [];
  eventSubscription: BehaviorSubject<EventItem> = new BehaviorSubject<EventItem>(null);

  constructor(private cognitoUtil: CognitoUtil) { }

  getUserId(): string {
    return this.cognitoUtil.getCognitoIdentity();
  }

  getRandomComponent(components: Type<any>[]): Type<any> {
    // TODO: Make this based on Cognito, or track which one was used with Cognito
    return components[Math.ceil(Math.random() * components.length) - 1];
  }

  recordEvent(event: Event, jobId: number, abId?: string) {
    const eventItem = {
      abId: abId,
      jobId: jobId,
      event: event,
      timestamp: new Date()
    };
    this.events.push(eventItem);
    this.eventSubscription.next(eventItem);
  }
}
