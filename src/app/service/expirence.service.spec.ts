import { TestBed, inject } from '@angular/core/testing';

import { ExpirenceService } from './expirence.service';

describe('ExpirenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExpirenceService]
    });
  });

  it('should be created', inject([ExpirenceService], (service: ExpirenceService) => {
    expect(service).toBeTruthy();
  }));
});
