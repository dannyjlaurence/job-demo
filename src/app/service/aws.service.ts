import {Injectable} from '@angular/core';
import {Callback, CognitoUtil} from './cognito.service';
import * as AWS from 'aws-sdk/global';

/**
 * https://github.com/awslabs/aws-cognito-angular-quickstart
 * Created by Vladimir Budilov
 * Adapted by Danny Laurence
 */

// declare var AMA: any;

@Injectable()
export class AwsUtil {
    public static firstLogin = true;
    public static runningInit = false;

    constructor(public cognitoUtil: CognitoUtil) {
        AWS.config.region = CognitoUtil._REGION;
    }

    static getCognitoParametersForIdConsolidation(idTokenJwt: string): {} {
      console.log('AwsUtil: enter getCognitoParametersForIdConsolidation()');
      const url = 'cognito-idp.' + CognitoUtil._REGION.toLowerCase() + '.amazonaws.com/' + CognitoUtil._USER_POOL_ID;
      const logins: Array<string> = [];
      logins[url] = idTokenJwt;
      const params = {
          IdentityPoolId: CognitoUtil._IDENTITY_POOL_ID, /* required */
          Logins: logins
      };

      return params;
    }

    /**
     * This is the method that needs to be called in order to init the aws global creds
     */
    initAwsService(callback: Callback, isLoggedIn: boolean, idToken: string) {

        if (AwsUtil.runningInit) {
            // Need to make sure I don't get into an infinite loop here, so need to exit if this method is running already
            console.log('AwsUtil: Aborting running initAwsService()...it\'s running already.');
            // instead of aborting here, it's best to put a timer
            if (callback != null) {
                callback.callback();
                callback.callbackWithParam(null);
            }
            return;
        }

        console.log('AwsUtil: Running initAwsService()');
        AwsUtil.runningInit = true;
        this.setupAWS(isLoggedIn, callback, idToken);
    }


    /**
     * Sets up the AWS global params
     *
     * @param isLoggedIn
     * @param callback
     */
    setupAWS(isLoggedIn: boolean, callback: Callback, idToken: string): void {
        console.log('AwsUtil: in setupAWS()');

        this.addCognitoCredentials(idToken);
        if (callback != null) {
            callback.callback();
            callback.callbackWithParam(null);
        }

        AwsUtil.runningInit = false;
    }

    addCognitoCredentials(idTokenJwt?: string): void {
      console.log('AwsUtil: in addCognitoCredentials()');
      let creds;
      if (idTokenJwt) {
        creds = this.cognitoUtil.buildCognitoCreds(idTokenJwt);
      } else {
        creds = this.cognitoUtil.buildDefaultCognitoCreds();
      }

      AWS.config.credentials = creds;

      creds.get(function (err) {
        console.log('AwsUtil: in addCognitoCredentials(), recieved credentials');
          if (!err) {
              if (AwsUtil.firstLogin) {
                console.log(AWS.config.credentials['identityId']);
                  AwsUtil.firstLogin = false;
              }
          } else {
            console.error(err);
          }
      });
    }
}
