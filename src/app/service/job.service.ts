import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Job } from 'src/app/model/job';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  job: Job = {
    id: 1,
    title: 'HVAC Tech',
    description: {
      // tslint:disable-next-line:max-line-length
      overall: 'We are looking for reliable, professional, motivated and hard working people. Candidates must be looking for a career with a stable HVACR company. We are a family oriented business providing HVAC and Refrigeration services since 1975. We install and service all types of heating, refrigeration and air conditioning equipment.',
      qualifications: `
        <ul>
          <li>Motivated Individual</li>
          <li>Must be groomed a neat appearance</li>
          <li>Friendly and personable</li>
          <li>Valid drivers license and clean driving record</li>
          <li>Must agree to background check and drug test</li>
          <li>If apprentice, must be enrolled in school to further your career</li>
        </ul>
      `,
      benefits: `
        <ul>
          <li>Competitive Wages</li>
          <li>Paid vacations and 9 holidays</li>
          <li>Medical, dental benefits</li>
          <li>Short Term/Long Term Disability and others available</li>
          <li>401(k) Plan with available company match</li>
          <li>Great working environment</li>
        </ul>
      `
    },
    requiredExpirences: [
      1, 2, 4
    ],
    relevantExpirences: [
      4, 1, 3, 2
    ]
  };

  jobs: Job[] = [];

  constructor() {
    for (var x = 0; x < 100; x++) {
      var jobClone = this.clone(this.job);
      jobClone.id = x + 1;
      jobClone.title = `${jobClone.title} ${x + 1}`;
      this.jobs.push(jobClone);
    }
  }

  clone<T>(obj:T): T {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

  getJobs(): Observable<Job[]> {
    return of(this.jobs);
  }

  getJob(id: number): Observable<Job> {
    return of(this.jobs.find((sjob) => id === sjob.id));
  }
}
