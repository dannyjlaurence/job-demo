import { Injectable } from '@angular/core';
import { Expirence } from '../model/expirence';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExpirenceService {

  expirence: {[id: number]: Expirence} = {
    1 : {
      id: 1,
      text: 'HVAC'
    },
    2: {
      id: 2,
      text: 'Mechanical'
    },
    3: {
      id: 3,
      text: 'Refrigeration'
    },
    4: {
      id: 4,
      text: 'Overall years of work experience'
    }
  };

  getExpirence(id: number): Observable<Expirence> {
    return of(this.expirence[id]);
  }
}
