export interface Job {
  id: number;
  title: string;
  description: JobDescription;
  requiredExpirences: number[];
  relevantExpirences: number[];
}

export interface JobDescription {
  qualifications: string;
  benefits: string;
  overall: string;
}
