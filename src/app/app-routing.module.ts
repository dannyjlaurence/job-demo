import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobsDashboardComponent } from 'src/app/component/jobs-dashboard/jobs-dashboard.component';
import { JobDetailComponent } from 'src/app/component/job-detail/job-detail.component';
import { JobApplicationComponent } from 'src/app/component/job-application/job-application.component';
import { JobConfirmationComponent } from 'src/app/component/job-confirmation/job-confirmation.component';
import { ReportsComponent } from './component/reports/reports.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'jobs',
    pathMatch: 'full'
  },
  {
    path: 'jobs',
    component: JobsDashboardComponent
  },
  {
    path: 'job/:id',
    component: JobDetailComponent,
  },
  {
    path: 'job/:id/apply',
    component: JobApplicationComponent,
  },
  {
    path: 'job/:id/confirm',
    component: JobConfirmationComponent,
  },
  {
    path: 'reports',
    component: ReportsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    enableTracing: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
