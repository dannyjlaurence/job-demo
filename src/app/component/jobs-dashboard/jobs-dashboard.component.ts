import { Component, OnInit } from '@angular/core';
import { JobService } from 'src/app/service/job.service';
import { Job } from 'src/app/model/job';

@Component({
  selector: 'jd-jobs-dashboard',
  templateUrl: './jobs-dashboard.component.html',
  styleUrls: ['./jobs-dashboard.component.scss']
})
export class JobsDashboardComponent implements OnInit {

  jobs: Job[];

  constructor(private jobsService: JobService) { }

  ngOnInit() {
    this.jobsService.getJobs().subscribe((jobs => this.jobs = jobs));
  }

}
