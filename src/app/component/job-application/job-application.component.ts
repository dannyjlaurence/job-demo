import { Component, OnInit } from '@angular/core';
import { UserService, Event } from 'src/app/service/user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { JobService } from 'src/app/service/job.service';
import { switchMap } from 'rxjs/operators';
import { Job } from 'src/app/model/job';
import { ExpirenceService } from 'src/app/service/expirence.service';

@Component({
  selector: 'jd-job-application',
  templateUrl: './job-application.component.html',
  styleUrls: ['./job-application.component.scss']
})
export class JobApplicationComponent implements OnInit {

  job: Job;
  sentFromAbId: string;

  constructor(public userService: UserService,
              private jobsService: JobService,
              public expirenceService: ExpirenceService,
              public router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.pipe(
      switchMap((params: ParamMap) => {
        return params['abId'];
      })
    ).subscribe((abId: string) => {
      this.sentFromAbId = abId;
    });
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const id = +params.get('id');
        return this.jobsService.getJob(id);
      })
    )
    .subscribe((job: Job) => {
      this.job = job;
    });
  }

  submit() {
    this.userService.recordEvent(Event.finishApplication, this.job.id, this.sentFromAbId);
    this.router.navigate(['job', this.job.id, 'confirm'], {
      queryParams: {
        abId: this.sentFromAbId
      }
    })
  }

}
