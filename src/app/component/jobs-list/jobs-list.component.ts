import { Component, OnInit, Input } from '@angular/core';
import { Job } from 'src/app/model/job';

@Component({
  selector: 'jd-jobs-list',
  templateUrl: './jobs-list.component.html',
  styleUrls: ['./jobs-list.component.scss']
})
export class JobsListComponent implements OnInit {

  @Input()
  jobs: Job[];

  constructor() { }

  ngOnInit() {
  }

}
