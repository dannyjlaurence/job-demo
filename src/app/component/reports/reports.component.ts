import { Component, OnInit } from '@angular/core';
import { UserService, Event } from 'src/app/service/user.service';

@Component({
  selector: 'jd-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  Event = Event;

  counts: {[abId: number]: number} = {};
  countArray = [];
  jobsSeen:  {[jobId: number]: number} = {};

  constructor(public userService: UserService) { }

  ngOnInit() {
    this.userService.eventSubscription.subscribe(() => {
      this.counts = {};
      this.countArray = [];
      this.jobsSeen = {};

      this.userService.events.forEach((event) => {
        if (event.abId && event.event === Event.finishApplication) {
          if (!this.counts[event.abId]) {
            this.counts[event.abId] = 1;
          } else {
            this.counts[event.abId]++;
          }
        }
        this.jobsSeen[event.jobId] = event.jobId;
      });
      let props = Object.keys(this.counts);
      for (let prop of props) {
        this.countArray.push({ id: prop, value:this.counts[prop]});
      }
    });
  }
}
