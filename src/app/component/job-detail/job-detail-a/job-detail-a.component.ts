import { Component, Input } from '@angular/core';
import { Job } from 'src/app/model/job';
import { ExpirenceService } from 'src/app/service/expirence.service';
import { AbstractJobDetailComponent } from 'src/app/component/job-detail/abstract-job-detail.component';
import { UserService } from '../../../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jd-job-detail-a',
  templateUrl: './job-detail-a.component.html',
  styleUrls: ['./job-detail-a.component.scss']
})
export class JobDetailAComponent extends AbstractJobDetailComponent {

  @Input()
  job: Job;

  constructor(public expirenceService: ExpirenceService, public userService: UserService, public router: Router) {
    super("a", expirenceService, userService, router);
   }
}
