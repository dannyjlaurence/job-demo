import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDetailAComponent } from './job-detail-a.component';

describe('JobDetailAComponent', () => {
  let component: JobDetailAComponent;
  let fixture: ComponentFixture<JobDetailAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobDetailAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDetailAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
