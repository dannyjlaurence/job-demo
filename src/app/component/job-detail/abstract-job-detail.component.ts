import { Component, Input, OnInit } from '@angular/core';
import { Job } from 'src/app/model/job';
import { ExpirenceService } from 'src/app/service/expirence.service';
import { UserService, Event } from '../../service/user.service';
import { Router } from '@angular/router';

export class AbstractJobDetailComponent implements OnInit {

  @Input()
  job: Job;

  constructor(public abId: string,
    public expirenceService: ExpirenceService,
    public userService: UserService,
    public router: Router) { }

  ngOnInit() {
    this.userService.recordEvent(Event.viewJob, this.job.id, this.abId);
  }

  startApplication() {
    this.userService.recordEvent(Event.startApplication, this.job.id, this.abId);
    this.router.navigate(['job', this.job.id, 'apply'], {
      queryParams: {
        abId: this.abId
      }
    });
  }
}
