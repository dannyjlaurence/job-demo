import { Component, OnInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Job } from 'src/app/model/job';

import { JobService } from 'src/app/service/job.service';
import { UserService } from 'src/app/service/user.service';
import { JobDetailAComponent } from './job-detail-a/job-detail-a.component';
import { JobDetailBComponent } from './job-detail-b/job-detail-b.component';
import { AbTestDirective } from '../../directive/ab-test.directive';

@Component({
  selector: 'jd-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss']
})
export class JobDetailComponent implements OnInit {

  @ViewChild(AbTestDirective)
  host: AbTestDirective;

  constructor(private jobsService: JobService,
              private userService: UserService,
              private route: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const id = +params.get('id');
        return this.jobsService.getJob(id);
      })
    )
    .subscribe((job: Job) => {
      this.loadComponent(job);
    });
  }

  loadComponent(job: Job) {
    const componentType = this.userService.getRandomComponent([JobDetailAComponent, JobDetailBComponent]);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    const viewContainerRef = this.host.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    // TODO: Strongly type the instance with an interface on the component
    componentRef.instance.job = job;
  }

}
