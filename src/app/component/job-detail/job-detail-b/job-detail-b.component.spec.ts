import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDetailBComponent } from './job-detail-b.component';

describe('JobDetailBComponent', () => {
  let component: JobDetailBComponent;
  let fixture: ComponentFixture<JobDetailBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobDetailBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDetailBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
