import { Component, OnInit, Input } from '@angular/core';
import { ExpirenceService } from 'src/app/service/expirence.service';
import { Job } from 'src/app/model/job';
import { AbstractJobDetailComponent } from 'src/app/component/job-detail/abstract-job-detail.component';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jd-job-detail-b',
  templateUrl: './job-detail-b.component.html',
  styleUrls: ['./job-detail-b.component.scss']
})
export class JobDetailBComponent extends AbstractJobDetailComponent {

  @Input()
  job: Job;

  constructor(public expirenceService: ExpirenceService, public userService: UserService, public router: Router) {
    super('b', expirenceService, userService, router);
   }
}
