import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { JobsDashboardComponent } from './component/jobs-dashboard/jobs-dashboard.component';
import { JobsListComponent } from './component/jobs-list/jobs-list.component';
import { JobDetailComponent } from './component/job-detail/job-detail.component';
import { JobDetailAComponent } from './component/job-detail/job-detail-a/job-detail-a.component';
import { JobDetailBComponent } from './component/job-detail/job-detail-b/job-detail-b.component';
import { AbTestDirective } from './directive/ab-test.directive';

import { CognitoUtil } from 'src/app/service/cognito.service';
import { AwsUtil } from 'src/app/service/aws.service';
import { UserRegistrationService } from 'src/app/service/user-registration.service';
import { UserLoginService } from 'src/app/service/user-login.service';
import { UserParametersService } from 'src/app/service/user-parameters.service';
import { JobApplicationComponent } from 'src/app/component/job-application/job-application.component';
import { JobConfirmationComponent } from './component/job-confirmation/job-confirmation.component';
import { ReportsComponent } from './component/reports/reports.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    JobsDashboardComponent,
    JobsListComponent,
    JobDetailComponent,
    JobDetailAComponent,
    JobDetailBComponent,
    AbTestDirective,
    JobApplicationComponent,
    JobConfirmationComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    CognitoUtil,
    AwsUtil,
    UserRegistrationService,
    UserLoginService,
    UserParametersService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    JobDetailAComponent,
    JobDetailBComponent
  ]
})
export class AppModule { }
